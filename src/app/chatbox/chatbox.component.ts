import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ChatboxService } from '../service/chatbox/chatbox.service';
import { UserService } from '../service/user/user.service';
import { User } from '../interface/user';
import { Message } from '../interface/message';
import { CommonModule } from '@angular/common';
import { ChatboxMessageComponent } from './chatbox-message/chatbox-message.component';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ChatboxMessageReactionsComponent } from './chatbox-message/chatbox-message-reactions/chatbox-message-reactions.component';
import { Icon } from '../interface/icon';

@Component({
  selector: 'app-chatbox',
  standalone: true,
  imports: [CommonModule, ChatboxMessageComponent, ReactiveFormsModule, ChatboxMessageReactionsComponent],
  templateUrl: './chatbox.component.html',
  styleUrl: './chatbox.component.scss'
})
export class ChatboxComponent {
  @Input() user!: User;

  readonly messages$ = this.chatboxService.messages$;

  readonly form = this.fb.group({
    text: this.fb.control<string>('', Validators.required)
  });

  reactionsPosition: { top: number; left: number } = { left: 0, top: 0};
  selectedMessage: Message | null = null;
  selectedIcon: string | undefined = undefined;

  constructor(private chatboxService: ChatboxService, private fb: FormBuilder) {}

  @HostListener('document:mousedown', ['$event'])
  unselect(event: MouseEvent): void {
    let target = event.target as any;
    
    do {
      if (target.tagName === 'APP-CHATBOX-MESSAGE-REACTIONS') return;
      target = target.parentElement;
    } while (target);

    this.selectedMessage = null;
  }

  sendMessage(): void {
    const text = this.form.getRawValue().text!;
    const user = {
      name: this.user.name,
      id: this.user.id
    };
    const message: Message = {
      text,
      user,
      date: new Date(),
      reactions: []
    };
    this.chatboxService.messages$.next([
      ...this.chatboxService.messages$.getValue(),
      message
    ]);
  }

  displayReactions(event: PointerEvent, message: Message): void {
    this.selectedMessage = message;
    this.reactionsPosition.left = event.x;
    this.reactionsPosition.top = event.y;
    this.selectedIcon = message.reactions.find((v) => v.user.id === this.user.id)?.icon.src;
  }

  addReaction(icon: Icon): void {
    const reactionIndex = this.selectedMessage!.reactions.findIndex((reaction) => reaction.user.id === this.user.id);
    if (reactionIndex > -1) {
      if (this.selectedMessage!.reactions[reactionIndex].icon === icon) {
        const oldReactions = this.selectedMessage!.reactions;
        const newReactions = oldReactions.slice(0, reactionIndex);
        if (oldReactions.length > reactionIndex + 1) {
          newReactions.push(...oldReactions.slice(reactionIndex + 1, oldReactions.length));
        }
        this.selectedMessage!.reactions = newReactions;
      } else {
        this.selectedMessage!.reactions[reactionIndex].icon = icon;
      }
    } else {
      this.selectedMessage!.reactions.push({icon, user: this.user})
    }
    this.selectedMessage = null;
  }
}
