import { Component, Input } from '@angular/core';
import { Message } from '../../interface/message';
import { CommonModule, DatePipe } from '@angular/common';
import { AngularSvgIconModule, SvgHttpLoader, SvgIconRegistryService, SvgLoader } from 'angular-svg-icon';
import { User } from '../../interface/user';
import { Icon } from '../../interface/icon';

@Component({
  selector: 'app-chatbox-message',
  standalone: true,
  imports: [DatePipe, CommonModule, AngularSvgIconModule],
  providers: [SvgIconRegistryService, {
    provide: SvgLoader,
    useClass: SvgHttpLoader
  }],
  templateUrl: './chatbox-message.component.html',
  styleUrl: './chatbox-message.component.scss'
})
export class ChatboxMessageComponent {
  @Input({required: true}) message!: Message;
  @Input() userId!: number;

  get messageReactions() {
    const reactions: {icon: Icon; users: User[]}[] = [];
    this.message.reactions.forEach((reaction) => {
      let index = reactions.findIndex((r) => r.icon.src === reaction.icon.src);
      if (index > -1) {
        reactions[index].users.push(reaction.user);
      } else {
        reactions.push({icon: reaction.icon, users: [reaction.user]});
      }
    });
    return reactions;
  }

  getSelectedReaction(reaction: { users: User[]}): boolean {
    return reaction.users.some((user) => user.id === this.userId);
  }
}
