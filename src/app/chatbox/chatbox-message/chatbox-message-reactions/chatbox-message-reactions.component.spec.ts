import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatboxMessageReactionsComponent } from './chatbox-message-reactions.component';

describe('ChatboxMessageReactionsComponent', () => {
  let component: ChatboxMessageReactionsComponent;
  let fixture: ComponentFixture<ChatboxMessageReactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChatboxMessageReactionsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChatboxMessageReactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
