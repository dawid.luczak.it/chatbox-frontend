import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AngularSvgIconModule, SvgHttpLoader, SvgIconRegistryService, SvgLoader } from 'angular-svg-icon';
import { icons } from '../../../constants/icons';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Icon } from '../../../interface/icon';

@Component({
  selector: 'app-chatbox-message-reactions',
  standalone: true,
  imports: [AngularSvgIconModule, CommonModule, HttpClientModule],
  providers: [SvgIconRegistryService, {
    provide: SvgLoader,
    useClass: SvgHttpLoader
  }],
  templateUrl: './chatbox-message-reactions.component.html',
  styleUrl: './chatbox-message-reactions.component.scss'
})
export class ChatboxMessageReactionsComponent {
  @Input() selectedIcon: string | undefined = undefined;
  @Output() select = new EventEmitter();

  readonly icons = icons;

  selectIcon(icon: Icon): void {
    this.select.emit(icon);
  }
}
