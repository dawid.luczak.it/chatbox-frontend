import { Component, EventEmitter, Output } from '@angular/core';
import { UserService } from '../service/user/user.service';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { User } from '../interface/user';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  @Output() login = new EventEmitter<string>();
  
  readonly form = this.fb.group({
    name: this.fb.control<string>('', Validators.required)
  });

  constructor(private fb: FormBuilder) {}

  doLogin(): void {
    if (this.form.invalid) {
      return;
    }
    const form = this.form.getRawValue();
    this.login.next(form.name!);
  }
}
