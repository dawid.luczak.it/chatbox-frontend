import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Message } from '../../interface/message';

@Injectable({
  providedIn: 'root'
})
export class ChatboxService {
  readonly messages$ = new BehaviorSubject<Message[]>([]);
}
