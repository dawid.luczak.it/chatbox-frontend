import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../interface/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly user$ = new BehaviorSubject<User | null>(null);
  
  readonly users$ = new BehaviorSubject<User[]>([]);

  login(name: string): void {
    const users = this.users$.getValue();
    let user = users.find((u) => u.name === name);
    if (!user) {
      user = { name, id: users.length + 1 };
      this.users$.next([
        ...users,
        user
      ]);
    }
    this.user$.next(user);
  }

  logout(): void {
    this.user$.next(null);
  }
}
