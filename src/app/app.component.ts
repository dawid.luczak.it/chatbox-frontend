import { UserService } from './service/user/user.service';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatboxComponent } from './chatbox/chatbox.component';
import { LoginComponent } from './login/login.component';
import { User } from './interface/user';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, ChatboxComponent, LoginComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  protected readonly user$ = this.userService.user$;
  protected readonly users$ = this.userService.users$;

  protected showUsers = false;
  protected selectedUser: User | null = null;

  constructor(private userService: UserService) {}

  logout(): void {
    this.userService.logout();
  }

  login(name: string): void {
    this.userService.login(name);
  }

  showUserList(): void {
    this.showUsers = !this.showUsers;
    if (this.showUsers === true) {
      this.selectedUser = this.user$.getValue();
    }
  }

  hideUserList(): void {
    this.showUsers = false;
  }

  selectUser(user: User): void {
    this.selectedUser = user;
  }
}
