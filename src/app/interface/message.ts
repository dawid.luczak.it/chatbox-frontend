import { MessageReaction } from "./message-reaction";
import { User } from "./user";

export interface Message {
  user: User;
  date: Date;
  text: string;
  reactions: MessageReaction[];
}
