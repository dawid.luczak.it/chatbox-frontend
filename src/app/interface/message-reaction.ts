import { Icon } from "./icon";
import { User } from "./user";

export interface MessageReaction {
  user: User;
  icon: Icon;
}
